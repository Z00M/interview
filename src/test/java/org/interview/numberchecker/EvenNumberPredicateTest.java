package org.interview.numberchecker;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class EvenNumberPredicateTest  {

    private static final Integer EVEN_NUMBER = 2;
    private static final Integer ODD_NUMBER = 3;

    private EvenNumberPredicate evenNumberPredicate;

    @Before
    public void init() {
        evenNumberPredicate = new EvenNumberPredicate();
    }

    @Test
    public void test_should_return_true_when_number_is_even() {
        boolean testResult = evenNumberPredicate.test(EVEN_NUMBER);

        assertThat(testResult, is(true));
    }

    @Test
    public void test_should_return_false_when_number_is_odd() {
        boolean testResult = evenNumberPredicate.test(ODD_NUMBER);

        assertThat(testResult, is(false));
    }
}
