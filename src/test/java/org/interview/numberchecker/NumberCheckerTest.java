package org.interview.numberchecker;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.function.Predicate;

import static org.mockito.Mockito.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class NumberCheckerTest {

    private static final int TEST_NUMBER = 10;

    private NumberChecker numberChecker;

    @Mock
    private Predicate<Integer> predicate;

    @Before
    public void init() {
        numberChecker = new NumberChecker(TEST_NUMBER, predicate);

        when(predicate.test(TEST_NUMBER)).thenReturn(true);
    }

    @Test
    public void test_should_call_function_only() {
        boolean result = numberChecker.isMetCondition();

        assertThat(result, is(true));
        verify(predicate).test(TEST_NUMBER);
        verifyNoMoreInteractions(predicate);
    }
}
