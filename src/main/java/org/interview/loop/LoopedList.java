package org.interview.loop;

import java.util.HashSet;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class LoopedList<T> {
    private Node<T> head;

    public boolean isLooped() {
        HashSet<T> set = new HashSet<>();

        Node<T> currentNode = head;
        if (isNull(currentNode)) {
            return false;
        }

        while (nonNull(currentNode)) {
            if (!set.add(currentNode.item)) {
                return true;
            }
            currentNode = currentNode.nextItem;
        }

        return false;
    }

    public void loopMySelf() {
        Node<T> lastNode = head;
        while (nonNull(lastNode.nextItem)) {
            lastNode = lastNode.nextItem;
        }
        lastNode.nextItem = head;
    }

    public void addItem(T item) {
        if (isNull(head)) {
            head = new Node<>(item, null);
            return;
        }

        Node<T> lastNode = head;
        while (nonNull(lastNode.nextItem)) {
            lastNode = lastNode.nextItem;
        }
        lastNode.nextItem = new Node<>(item, null);

    }

    private static class Node<T> {
        private T item;
        private Node<T> nextItem;

        public Node(T item, Node<T> nextItem) {
            this.item = item;
            this.nextItem = nextItem;
        }
    }

    public static void main(String[] args) {
        LoopedList<String> loopedList = new LoopedList<>();
        loopedList.addItem("a");
        loopedList.addItem("a1");
        loopedList.addItem("a2");
        loopedList.addItem("a3");
        loopedList.addItem("a4");
        System.out.println(loopedList.isLooped());
        loopedList.loopMySelf();
        System.out.println(loopedList.isLooped());
    }
}
