package org.interview.numberchecker;

import java.util.function.Predicate;

public class NumberChecker {

    private int numberToCheck;
    private Predicate<Integer> function;

    public NumberChecker(int numberToCheck, Predicate<Integer> function) {
        this.numberToCheck = numberToCheck;
        this.function = function;
    }

    public boolean isMetCondition() {
        return function.test(numberToCheck);
    }
}
