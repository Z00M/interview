package org.interview.sortitems;

public class Car implements Sortable{
    private String colour;
    private String modelName;

    public Car(String colour, String modelName) {
        this.colour = colour;
        this.modelName = modelName;
    }

    @Override
    public String getName() {
        return getModelName();
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
