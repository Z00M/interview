package org.interview.sortitems;

import java.util.Comparator;
import java.util.List;

public class Sorter {

    private List<Sortable> list;
    private Comparator<Sortable> comparator;

    public Sorter(List<Sortable> list, Comparator<Sortable> comparator) {
        this.list = list;
        this.comparator = comparator;
    }

    public List<Sortable> sort() {
        list.sort(comparator);
        return list;
    }

}
