package org.interview.sortitems;

public class Tree implements Sortable{
    private String colour;
    private String treeName;

    public Tree(String colour, String treeName) {
        this.colour = colour;
        this.treeName = treeName;
    }

    @Override
    public String getName() {
        return getTreeName();
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }
}
