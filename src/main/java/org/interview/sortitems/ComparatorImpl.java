package org.interview.sortitems;

import java.util.Comparator;

public class ComparatorImpl implements Comparator<Sortable> {

    @Override
    public int compare(Sortable o1, Sortable o2) {
        int nameComparingResult = o1.getName().compareTo(o2.getName());
        if (nameComparingResult != 0) {
            return nameComparingResult;
        }

        return o1.getColour().compareTo(o2.getColour());
    }
}
