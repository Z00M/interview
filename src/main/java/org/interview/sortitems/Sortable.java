package org.interview.sortitems;

public interface Sortable {

    String getColour();
    String getName();
}
